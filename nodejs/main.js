const express = require('express')
var mysql = require('mysql');
const app = express()
const port = 3000

app.set('view engine', 'ejs');

var dataresult = {};


var connection = mysql.createConnection({
  host     : 'mysql',
  user     : 'root',
  password : 'root',
  database : 'info'
});

connection.connect()

connection.query('SELECT * FROM helloworld', function (err, rows, fields) {
  if (err) throw err
   var rows = JSON.parse(JSON.stringify(rows[0]))
   var parsed = JSON.stringify(rows)
   
   dataresult = parsed
})

connection.end()

// index page 
app.get('/', function(req, res, parsed) {
    var htmldata =  dataresult
    res.render('public/index', {
        htmldata: htmldata
    });
});


//app.get('/', (req, res) => res.send('Hello World!'))

app.listen(port, () => console.log(`Example app listening on!`))